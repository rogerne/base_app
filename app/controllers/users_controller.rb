class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy]
  before_action :correct_user,   only: [:show, :edit, :update] 
  
  def new
  end

  def show
  	@user = User.find(params[:id])
  	#debugger
  end

  def new
  	 @user = User.new
  end

  def create  
    puts "Create triggered"
    @user = params[:user] ? User.new(user_params) : User.new_guest
    if @user.save
      if @user.guest
        #Uncomment this section if you are not using account activation email
        log_in @user
        flash[:success] = "Welcome to the Base App!"      
        redirect_to user_path(@user)
      else
        #Uncomment this section if you are using account activation email
        UserMailer.account_activation(@user).deliver_now
        flash[:info] = "Please check your email to activate your account."
        redirect_to root_url
      end
    else
      render 'new'
    end
  end

  def edit
    puts "Trying to edit"
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def index
    @users = User.all
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def logged_in_user
      puts "See if this user is logged in"

      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    def correct_user
      puts "See if this is the correct user"
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
